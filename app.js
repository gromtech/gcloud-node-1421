'use strict';

var express = require('express');
var gcloud = require('gcloud');
var app = express();

var projectId = "project-id"; // Need to be changed!!!!

function tryToSaveEntity(callback) {

	var datastore = gcloud.datastore({
		projectId: projectId
	});

	var entities = [
		{
			"key": datastore.key(["CD_Campaign", "120"]),
			"data": {
				"persons": 20,
				"attempted": 0,
				"contacted": 0,
				"bounds": {
					"min": {
						"lng": -122.50355,
						"lat": 37.70851
					},
					"max": {
						"lng": -122.43805,
						"lat": 37.7907
					}
				},
				"disp": {
					"NH": 0,
					"LD": 0,
					"VC": 0,
					"LB": 0,
					"D": 0,
					"NC": 0,
					"IN": 0,
					"DC": 0,
					"RF": 0,
					"MV": 0,
					"WN": 0,
					"AV": 0,
					"V": 0,
					"B": 0,
					"LM": 0,
					"CB": 0
				},
				"areaIds": [
					"0500000US06075"
				]
			}
		}
	];


	datastore.save(entities, function (err, apiResponse) {
		callback(err);
	});
}

app.get('/', function (req, res) {
	tryToSaveEntity(function (err) {
		if (!err) {
			res.status(200).send("SUCCESS");
		} else {
			res.status(400).send("FAIL");
		}
	});

});

if (module === require.main) {
	var server = app.listen(process.env.PORT || 8080, function () {
		var host = server.address().address;
		var port = server.address().port;

		console.log('App listening at http://%s:%s', host, port);
	});
}

module.exports = app;
